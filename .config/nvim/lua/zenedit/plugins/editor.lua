return {
  -- Bulk find and replace
  {
    "nvim-pack/nvim-spectre",
    cmd = "Spectre",
    opts = { open_cmd = "noswapfile vnew" },
    keys = {
      { "<leader>sr", function() require("spectre").open() end, desc = "Replace in files (Spectre)" },
    },
  },
  -- Delete Buffer easily
  {
    "echasnovski/mini.bufremove",
    keys = {
      { "<leader>bq", function() require("mini.bufremove").delete(0, false) end, desc = "Delete Buffer" },
      { "<C-q>", function() require("mini.bufremove").delete(0, false) end, desc = "Delete Buffer" },
    },
  }
}
