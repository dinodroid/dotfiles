return {
  {
    "catppuccin/nvim",
    name = "catppuccin",
    build = ":CatppuccinCompile",
    lazy = false,    -- make sure we load this during startup
    priority = 1000, -- make sure to load this before all the others
    opts = {
      flavour = "mocha",
      styles = {
        comments = { "italic" },
        conditionals = { "italic" },
      },
      transparent_background = false,
      term_colors = false,
      compile = {
        enabled = true,
        path = vim.fn.stdpath("cache") .. "/catppuccin",
        suffix = "_compiled",
      },
      dim_inactive = {
        enabled = false,
        shade = "dark",
        percentage = 0.15,
      },
      integrations = {
        alpha = true,
        bufferline = true,
        fidget = true,
        gitsigns = true,
        indent_blankline = {
          enabled = true,
          colored_indent_levels = true,
        },
        mason = true,
        mini = true,
        neotree = true,
        noice = true,
        cmp = true,
        native_lsp = {
          enabled = true,
          virtual_text = {
            errors = { "italic" },
            hints = { "italic" },
            warnings = { "italic" },
            information = { "italic" },
          },
          underlines = {
            errors = { "underline" },
            hints = { "underline" },
            warnings = { "underline" },
            information = { "underline" },
          },
        },
        -- navic = {
        --   enabled = true,
        --   custom_bg = "NONE",
        -- },
        notify = true,
        treesitter = true,
        -- ts_rainbow = true,
        telescope = true,
        -- lsp_trouble = true,
      },
    },
    config = function()
      -- load the colorscheme here
      vim.cmd([[colorscheme catppuccin]])
    end,
  },
}
