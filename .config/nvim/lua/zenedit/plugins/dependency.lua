return {
  {
    "nvim-lua/plenary.nvim",
    name = "plenary",
  },
  {
    "nvim-tree/nvim-web-devicons",
    name = "devicons",
  },
  {
    "tpope/vim-repeat",
    name = "repeat"
  }
}
