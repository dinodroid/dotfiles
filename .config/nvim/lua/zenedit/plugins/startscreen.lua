return {
  {
    "goolord/alpha-nvim",
    event = "VimEnter",
    opts = function()
      local dashboard = require("alpha.themes.dashboard")
      local logo = [[
     (   ) )
      ) ( (
 zen_______)_
 .-'---------|
( C|/\/\/\/\/|
 '-./\/\/\/\/|
   '_________'
    '-------'
]]

      dashboard.section.header.val = vim.split(logo, "\n")
      dashboard.section.buttons.val = {
        dashboard.button("n", " " .. " [N]ew file", ":ene <BAR> startinsert <CR>"),
        dashboard.button("r", " " .. " [R]ecent files", ":FzfLua oldfiles <CR>"),
        dashboard.button("c", " " .. " [C]onfig", ":e $MYVIMRC <CR>"),
        dashboard.button("l", "󰒲 " .. " [L]azy", ":Lazy<CR>"),
        dashboard.button("s", " " .. " Restore [S]ession", [[:lua require("persistence").load({ last = true }) <cr>]]),
        dashboard.button("q", " " .. " [Q]uit", ":qa<CR>"),
      }
      dashboard.opts.layout[1].val = 8
      return dashboard
    end,
    config = function(_, dashboard)
      -- close Lazy and re-open when the dashboard is ready
      if vim.o.filetype == "lazy" then
        vim.cmd.close()
        vim.api.nvim_create_autocmd("User", {
          pattern = "AlphaReady",
          callback = function()
            require("lazy").show()
          end,
        })
      end

      require("alpha").setup(dashboard.opts)

      vim.api.nvim_create_autocmd("User", {
        pattern = "LazyVimStarted",
        callback = function()
          local stats = require("lazy").stats()
          local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
          dashboard.section.footer.val = "⚡ ZenVim loaded " .. stats.count .. " plugins in " .. ms .. "ms"
          pcall(vim.cmd.AlphaRedraw)
        end,
      })
    end,
  }
}
