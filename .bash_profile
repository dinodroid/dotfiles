#
# ~/.bash_profile
#
# Set ~/.bashrc

[[ -f ~/.bashrc ]] && . "$HOME/.bashrc"

if [[ "$(tty)" = "/dev/tty1" ]]; then
	pgrep qtile || startx "$HOME/.xinitrc"
fi

