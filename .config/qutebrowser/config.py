# ┏━┓╻ ╻╺┳╸┏━╸┏┓ ┏━┓┏━┓╻ ╻┏━┓┏━╸┏━┓   ┏━╸┏━┓┏┓╻┏━╸╻┏━╸
# ┃┓┃┃ ┃ ┃ ┣╸ ┣┻┓┣┳┛┃ ┃┃╻┃┗━┓┣╸ ┣┳┛   ┃  ┃ ┃┃┗┫┣╸ ┃┃╺┓
# ┗┻┛┗━┛ ╹ ┗━╸┗━┛╹┗╸┗━┛┗┻┛┗━┛┗━╸╹┗╸   ┗━╸┗━┛╹ ╹╹  ╹┗━┛

# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

# Imports

import subprocess
import os
from qutebrowser.api import interceptor

config.load_autoconfig(False)

# Type: Dict
c.aliases = {'q': 'quit', 'w': 'session-save', 'wq': 'quit --save'}
# Setting dark mode
config.set("colors.webpage.darkmode.enabled", True)

config.set('content.cookies.accept', 'no-3rdparty', 'chrome-devtools://*')
config.set('content.cookies.accept', 'no-3rdparty', 'devtools://*')

config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://web.whatsapp.com/')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version} Edg/{upstream_browser_version}', 'https://accounts.google.com/*')

# Load images automatically in web pages.
# Type: Bool
config.set('content.images', True, 'chrome-devtools://*')
config.set('content.images', True, 'devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'chrome-devtools://*')
config.set('content.javascript.enabled', True, 'devtools://*')
config.set('content.javascript.enabled', True, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')

config.set('content.notifications.enabled', False)
config.set('content.private_browsing', True)


c.downloads.location.directory = '~/Downloads'

config.set('statusbar.show', 'never')
config.set('tabs.show', 'never')
config.set('url.start_pages', 'file:///home/dinodroid/browser/home.html')
config.set('url.default_page', 'file:///home/dinodroid/browser/home.html')

config.set('url.searchengines', {
    'DEFAULT': 'https://duckduckgo.com/?q={}', 
    "am": "https://www.amazon.com/s?k={}", 
    "aw": "https://wiki.archlinux.org/?search={}", 
    "g": "https://www.google.com/search?q={}", 
    "rdd": "https://www.reddit.com/r/{}", 
    "ub": "https://www.urbandictionary.com/define.php?term={}", 
    "wiki": "https://en.wikipedia.org/wiki/{}", 
    "yt": "https://www.youtube.com/results?search_query={}"
})
config.set('content.headers.accept_language', 'en-US,en;q=0.5')
config.set('content.headers.custom', {"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"})

# taken from https://qutebrowser.org/doc/help/configuring.html
def read_xresources(prefix):
    """
    read settings from xresources
    """
    props = {}
    x = subprocess.run(["xrdb", "-query"], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split("\n")
    for line in filter(lambda l: l.startswith(prefix), lines):
        prop, _, value = line.partition(":\t")
        props[prop] = value
    return props

xresources = read_xresources("*")

c.colors.statusbar.normal.bg = xresources["*background"]
c.colors.statusbar.command.bg = xresources["*background"]
c.colors.statusbar.command.fg = xresources["*foreground"]
c.colors.statusbar.normal.fg = xresources["*foreground"]

c.colors.tabs.even.bg = xresources["*background"]
c.colors.tabs.odd.bg = xresources["*background"]
c.colors.tabs.even.fg = xresources["*foreground"]
c.colors.tabs.odd.fg = xresources["*foreground"]
c.colors.tabs.selected.even.bg = xresources["*color8"]
c.colors.tabs.selected.odd.bg = xresources["*color8"]
c.colors.hints.bg = xresources["*background"]
c.colors.hints.fg = xresources["*foreground"]
c.tabs.show = "multiple"

c.colors.tabs.indicator.stop = xresources["*color14"]
c.colors.completion.odd.bg = xresources["*background"]
c.colors.completion.even.bg = xresources["*background"]
c.colors.completion.fg = xresources["*foreground"]
c.colors.completion.category.bg = xresources["*background"]
c.colors.completion.category.fg = xresources["*foreground"]
c.colors.completion.item.selected.bg = xresources["*background"]
c.colors.completion.item.selected.fg = xresources["*foreground"]

config.set('fonts.default_family', 'JetBrainsMono Nerd Font Mono')
# Fixing Annoyances
config.set('tabs.last_close', 'close')
config.set('content.notifications.enabled', False)
config.set('content.autoplay', False)
# Hardening
config.set('content.headers.user_agent', 'Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0')
config.set('content.autoplay', False)
config.set('content.canvas_reading', False)
config.set('content.cookies.accept', 'no-3rdparty')
config.set('content.default_encoding', 'utf-8')
config.set('content.dns_prefetch', False)
config.set('content.geolocation', False)
config.set('content.headers.do_not_track', False)
config.set('content.webgl', False)
config.set('content.webrtc_ip_handling_policy', 'default-public-interface-only')
config.set('content.local_content_can_access_remote_urls', True)
config.set('content.blocking.adblock.lists', ["https://easylist.to/easylist/easylist.txt","https://easylist.to/easylist/easyprivacy.txt", "https://raw.githubusercontent.com/neodevpro/neodevhost/master/lite_adblocker","https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt","https://blocklistproject.github.io/Lists/ads.txt" ])

config.set('content.blocking.hosts.lists', [ "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts", "https://blocklistproject.github.io/Lists/abuse.txt", "https://blocklistproject.github.io/Lists/crypto.txt", "https://blocklistproject.github.io/Lists/drugs.txt", "https://blocklistproject.github.io/Lists/fraud.txt", "https://blocklistproject.github.io/Lists/gambling.txt", "https://blocklistproject.github.io/Lists/malware.txt", "https://blocklistproject.github.io/Lists/phishing.txt", "https://blocklistproject.github.io/Lists/ransomware.txt", "https://blocklistproject.github.io/Lists/scam.txt", "https://blocklistproject.github.io/Lists/tracking.txt"])
# Bindings for normal mode
config.bind('M', 'hint links spawn mpv {hint-url}')
config.bind('Z', 'hint links spawn alacritty -e youtube-dl {hint-url}')
config.bind('t', 'set-cmd-text -s :open -t')
config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xh', 'history-clear')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')
config.bind('<Backspace>', 'back')


# Pause youtube ads
def filter_yt(info: interceptor.Request):
	"""Block the given request if necessary."""
	url = info.request_url
	if (url.host() == 'www.youtube.com' and
			url.path() == '/get_video_info' and
			'&adformat=' in url.query()):
		info.block()


interceptor.register(filter_yt)
