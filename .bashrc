#!/bin/bash
#/\/|  ___              | |
#|/\/  / / |__   __ _ ___| |__  _ __ ___
#     / /| '_ \ / _` / __| '_ \| '__/ __|
#    / /_| |_) | (_| \__ \ | | | | | (__
#   /_/(_)_.__/ \__,_|___/_| |_|_|  \___|


#--------------------------> SET VARIABLES <--------------------------------

# Default Apps
export TERMINAL="alacritty"
export EDITOR="nvim"
export VISUAL="nvim"
export BROWSER="librewolf"
export TRUE_BROWSER="librewolf"
export VIDEO="mpv"
export IMAGE="sxiv -a"
export COLORTERM="truecolor"
export OPENER="linkhandler"
export PAGER="less"
export WM="qtile"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
# Other XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_STATE_HOME=${XDG_CONFIG_HOME:="$HOME/.local/state"}

# PATH OF MY SCRIPTS
export PATH="$PATH:$HOME/.scripts:$HOME/.local/bin"

# for fzf catpuccin colors
export FZF_DEFAULT_OPTS='--color=bg+:#302D41,bg:#1E1E2E,spinner:#F8BD96,hl:#F28FAD --color=fg:#D9E0EE,header:#F28FAD,info:#DDB6F2,pointer:#F8BD96 --color=marker:#F8BD96,fg+:#F2CDCD,prompt:#DDB6F2,hl+:#F28FAD'
# for micro truecolor
export MICRO_TRUECOLOR="1"

#export LD_LIBRARY_PATH="/usr/lib/"

# Control History
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000
shopt -s histappend
shopt -s cmdhist

#---------------------------> BASH ALIASES <------------------------
[ -f "$HOME/.bash_aliases" ] && source "$HOME/.bash_aliases"

[[ $- != *i* ]] && return

# Disables CTRL+S and CTRL+Q
stty -ixon

# Allows to Cd if accidently typed directory name
shopt -s autocd

# Case-insensitive tab completetion
bind 'set completion-ignore-case on'
# Show tab completion options on the first press of TAB
bind 'set show-all-if-ambiguous on'
bind 'set show-all-if-unmodified on'

# Bash Prompt
PS1='\n \[\e[0;38;5;25m\][ \[\e[0;38;5;246m\]\A \[\e[0;38;5;32m\]\w \[\e[0;38;5;26m\]] \[\e[0;1;5;93m\]\n\$ \[\e[0m\]'
#eval "$(starship init bash)"
randomquote

