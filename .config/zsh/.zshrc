#!/bin/zsh

# Declare config DIR
export ZDOTDIR="$HOME/.config/zsh"

function source_file() {
  [ -f "$1" ] && source "$1"
}

# Local Sources
source_file "$ZDOTDIR/zsh-exports.zsh" # Exports
source_file "$ZDOTDIR/zsh-aliases.zsh" # Aliases

# Created by Zap installer
source_file "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh"

# Zap Plugins
plug "zsh-users/zsh-autosuggestions"
plug "zap-zsh/supercharge"
# plug "zap-zsh/zap-prompt"
plug "zsh-users/zsh-syntax-highlighting"

# Load and initialise completion system
autoload -Uz compinit
compinit

# add zoxide support
eval "$(zoxide init zsh)"

if [ -z "$TMUX" ]; then
  # show randomquote
  randomquote
fi

# starship prompt
eval "$(starship init zsh)"
