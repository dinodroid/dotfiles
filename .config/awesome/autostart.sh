#!/bin/bash

function run {
    if ! pgrep "$1";
    then
        "$@"&
    fi
}

## ---------------- Essentials-------------------------

## Fix Input Controls
# Turn on Tap to Click
run ~/.scripts/taptoclick &
# Map Capslock to Esc for VIM
setxkbmap -option caps:escape
# Run Hotkey Daemon
run sxhkd &
# Run Network Manager
run nm-applet &
# Run PowerManager for battery
run xfce4-power-manager &
# Run polkit - session manager
run lxsession &
# Run Notification Daemon
# run dunst > /dev/null 2>&1 &
# Set Wallpaper
run xwallpaper --zoom ~/Pictures/wall.png &

##--------------------Optional-----------------------------------
# Run compositor
run picom &
# Run Redshift
run redshift -l 26.846695:80.946167 &
# Run Torrent-daemon
run transmission-daemon &
# Run music player daemon and stop music
run mpd & 
run mpc stop &

run polybar &
# Set NextDNS
# curl -s "https://link-ip.nextdns.io/4ac257/e3b9df461534b71f" &
#run mpv --no-video ~/sounds/startup.mp3 &







