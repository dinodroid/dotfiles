return {
  {
    "neovim/nvim-lspconfig",
    event = { "BufReadPost", "BufNewFile" },
    dependencies = {
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      "hrsh7th/cmp-nvim-lsp",
    },
    version = false,
    config = function()
      local servers_configs = {
        lua_ls = {
          settings = {
            Lua = {
              runtime = { version = 'LuaJIT', },
              diagnostics = { globals = { 'vim', }, },
              telemetry = { enable = false, },
            },
          }
        },
        intelephense = { single_file_support = true }
      }

      -- Fire on lsp_attach
      local function on_lsp_attach(event)
        -- local client = vim.lsp.get_client_by_id(event.data.client_id)
        local bufnr = event.buf
        local opts = { buffer = bufnr }
        vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
        vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
        vim.keymap.set({ 'n', 'x' }, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
        vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)
        vim.keymap.set('n', 'gl', '<cmd>lua vim.diagnostic.open_float()<cr>', opts)
        vim.keymap.set('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<cr>', opts)
        vim.keymap.set('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<cr>', opts)
        -- Ref: add something to get lsp info
      end

      local function configure_diagnostics()
        -- diagnostics icons
        for name, icon in pairs(require("zenedit.icons").icons.diagnostics) do
          name = "DiagnosticSign" .. name
          vim.fn.sign_define(name, { text = icon, texthl = name, numhl = "" })
        end
        vim.diagnostic.config({
          underline = true,
          update_in_insert = false,
          virtual_text = {
            spacing = 4,
            source = "if_many",
            prefix = "●",
          },
          severity_sort = true,
        })
      end

      local function setup_mason()
        local mason = require("mason")
        local mason_lspconfig = require("mason-lspconfig")
        -- Setup Mason and Mason Config
        mason.setup()
        mason_lspconfig.setup({
          ensure_installed = {
            'lua_ls',
            'html',
            'emmet_ls',
            'cssls',
            'tsserver',
            'intelephense',
            'gopls',
            'pyright',
            'clangd',
            'bashls'
          }
        })
      end

      local function setup_capabilities()
        -- Setup LSP capabilities

        local lspconfig = require('lspconfig')
        local cmp_nvim_lsp = require('cmp_nvim_lsp')
        local mason_lspconfig = require('mason-lspconfig')
        local lsp_capabilities = cmp_nvim_lsp.default_capabilities()
        local get_servers = mason_lspconfig.get_installed_servers

        for _, server_name in ipairs(get_servers()) do
          local server_opts = servers_configs[server_name] or {}
          server_opts.capabilities = vim.tbl_deep_extend("force", lsp_capabilities, server_opts.capabilities or {})
          lspconfig[server_name].setup(server_opts)
        end
      end


      setup_mason()
      setup_capabilities()
      -- Lazy load the LSP servers and dependencies
      vim.defer_fn(function()
        configure_diagnostics()
      end, 300)

      -- On Attach Function
      vim.api.nvim_create_autocmd('LspAttach', {
        desc = 'LSP actions',
        callback = on_lsp_attach
      })
    end
  },
}
