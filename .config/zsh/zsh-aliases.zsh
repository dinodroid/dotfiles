#--┏━┓╻  ╻┏━┓┏━┓┏━╸┏━┓
#--┣━┫┃  ┃┣━┫┗━┓┣╸ ┗━┓
#--╹ ╹┗━╸╹╹ ╹┗━┛┗━╸┗━┛

# Change Programs
alias nano='micro'
alias v='nvim'
alias vi='nvim'
alias vim='nvim'
alias yay='paru'
alias ll='exa -alh --color=always --group-directories-first --icons'
alias scat='highlight'

## Shortening
alias pkg='sudo pacman'
alias sys='sudo systemctl'
alias tnr='transmission-remote'
alias c='clear'

# Safety
alias rm='rm -I --preserve-root' ## Less Intrusive
alias mv='mv -i'
alias cp='cp -i'

# Default way
alias ls='ls -alhC --color=always --group-directories-first'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias mkdir='mkdir -pv'
alias df='df -h --total'
alias du='du -h --total'
alias free='free -gh'
alias ps='ps aux'
alias ping='ping -c 5'
alias journalctl='sudo journalctl -p 3 -xb'

#New
alias config='/usr/bin/git --git-dir=/home/dinodroid/.dotfiles/ --work-tree=/home/dinodroid'
alias ..='cd ..'
alias mirrors='sudo reflector --latest 10 --sort rate --save /etc/pacman.d/mirrorlist > /dev/null 2>&1 && notify-send "Mirrors updated."'
alias pkg-refresh='sudo pacman-key --refresh-keys && notify-send "Packages Keys Refreshed."'
alias pkg-clean='sudo pacman -Rns $(pacman -Qtdq)' # Remove orphan packages
alias wallx='xwallpaper --stretch $(sxiv -ato "~/Pictures/catppuccin")'

# Combine Programs
alias mpv='devour mpv'
alias sxiv='devour sxiv'

# External Alias
alias myip='curl https://ipinfo.io/ip'
alias weather='curl https://wttr.in'

# Youtube DL
alias youtube-dl="yt-dlp -N 32 --downloader 'aria2c'"
alias ytdlmp3list="youtube-dl -i --extract-audio --embed-thumbnail --add-metadata --audio-format 'mp3' -o '~/Music/%(playlist_title)s/%(title)s.%(ext)s'"
alias ytqvideodl="youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4"

alias vcreate="virtualenv venv"
alias vactivate="source venv/bin/activate"

alias aria2c="aria2c -x 32"

alias dl="axel -n 32"
alias dl2="aria2c --split=32"
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Git aliases
alias g='git'
alias gs='git status'
alias ga='git add'
alias gc='git commit'
alias gcm="git commit -m"
alias gp='git push'
alias gP='git pull'
alias gl='git log'
alias gr='git restore'
alias gu='git restore --staged'
alias gacm='git add . && git commit -m'

alias record-screen='ffmpeg -video_size 1920x1080 -framerate 25 -f x11grab -i :0.0 -f pulse -ac 2 -i default -c:v libx264 -preset ultrafast -c:a aac -b:a 128k -f mp4 "$(date +"%Y-%m-%d_%H-%M-%S").mp4"'

# New functions
hgrep() { history | grep -- "$1"; }
mkcd() { mkdir -p -- "$1" && cd -P -- "$1" || return; }
phpserve() { php -S "0.0.0.0:$1" -t "$2" & }
mkscript() { echo "#!$SHELL" > "$1" && sudo chmod u+x "$1" && $EDITOR "$1"; }
cheat() { curl "cheat.sh/$1"; }
restart_app() { pgrep "$1" && killall "$1"; sleep 1 ; "$1" &  }
dns() { ping -c 1 -- "$1" | awk NR==1'{print $3}' | sed 's/(//; s/)//'; }
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf "$1"   ;;
      *.tar.gz)    tar xzf "$1"   ;;
      *.bz2)       bunzip2 "$1"   ;;
      *.rar)       unrar x "$1"   ;;
      *.gz)        gunzip "$1"    ;;
      *.tar)       tar xf "$1"    ;;
      *.tbz2)      tar xjf "$1"   ;;
      *.tgz)       tar xzf "$1"   ;;
      *.zip)       unzip "$1"     ;;
      *.Z)         uncompress "$1";;
      *.7z)        7z x "$1"      ;;
      *.deb)       ar x "$1"      ;;
      *.tar.xz)    tar xf "$1"    ;;
      *.tar.zst)   unzstd "$1"    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
