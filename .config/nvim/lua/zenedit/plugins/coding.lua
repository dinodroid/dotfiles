return {
  -- Comment Support
  {
    "echasnovski/mini.comment",
    keys = { "gc", "gcc" },
    dependencies = {
      { "JoosepAlviste/nvim-ts-context-commentstring", lazy = true }
    },
    opts = {
      options = {
        custom_commentstring = function()
          return require("ts_context_commentstring.internal").calculate_commentstring() or vim.bo.commentstring
        end,
      },
    },
  },
  -- Surround Support
  {
    "echasnovski/mini.surround",
    keys = { "ys", "ds", "cs" },
    opts = {
      mappings = {
        add = "ys", -- Add surrounding in Normal and Visual modes
        delete = "ds", -- Delete surrounding
        replace = "cs", -- Replace surrounding
      },
    },
  },
}
