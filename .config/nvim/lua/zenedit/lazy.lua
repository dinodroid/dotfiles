--╻  ┏━┓╺━┓╻ ╻
--┃  ┣━┫┏━┛┗┳┛
--┗━╸╹ ╹┗━╸ ╹

-- Install Lazy if it is not installed
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local lazy_success, lazy = pcall(require, "lazy")
if not lazy_success then
  return
end

-- Optimized plugin setup and configuration
local disabled_plugins = {
  "2html_plugin",
  "bugreport",
  "compiler",
  "getscript",
  "getscriptPlugin",
  "gzip",
  "logipat",
  "matchit",
  "matchparen",
  "netrw",
  "netrwFileHandlers",
  "netrwPlugin",
  "netrwSettings",
  "optwin",
  "rplugin",
  "rrhelper",
  "spellfile_plugin",
  "synmenu",
  "syntax",
  "tar",
  "tarPlugin",
  "tohtml",
  "tutor",
  "vimball",
  "vimballPlugin",
  "zip",
  "zipPlugin",
}

lazy.setup({
  spec = {
    { import = "zenedit.plugins" },
  },
  defaults = {
    lazy = true,
    version = "*",
  },
  install = { colorscheme = { "catppuccin", "habamax" } },
  checker = { enabled = false },
  change_detection = {
    enabled = false,
    notify = false,
  },
  performance = {
    rtp = {
      disabled_plugins = disabled_plugins,
    },
  },
})
