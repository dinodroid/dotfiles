--╺━┓┏━╸┏┓╻┏━╸╺┳┓╻╺┳╸
--┏━┛┣╸ ┃┗┫┣╸  ┃┃┃ ┃
--┗━╸┗━╸╹ ╹┗━╸╺┻┛╹ ╹

require("zenedit.options")
require("zenedit.keymaps")
require("zenedit.lazy")
require("zenedit.autocmds")
