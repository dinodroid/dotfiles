return {
  {
    "nvim-treesitter/nvim-treesitter",
    name = "treesitter",
    event = { "BufReadPost", "BufNewFile" },

    build = function()
      local ts_update = require("nvim-treesitter.install").update({
        with_sync = false
      })
      ts_update()
    end,

    config = function(_, opts)
      require("nvim-treesitter.configs").setup(opts)
    end,

    opts = {
      auto_install = true,
      sync_install = false,
      highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
      },
      indent = {
        enable = true
      },
      context_commentstring = {
        enable = true -- Enable context aware comments (Commment ts plugin)
      },
      ensure_installed = { "bash", "c", "cpp", "css", "go", "html", "javascript",
        "json", "lua", "markdown", "markdown_inline", "php", "python", "rasi",
        "regex", "scss", "sql", "vim", "yaml" },
      rainbow = {
        enable = true,
        disable = { "jsx" },
      }
    }
  },
}
