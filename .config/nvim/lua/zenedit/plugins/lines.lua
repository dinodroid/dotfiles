return {
  --Staline and Stabline
  {
    "tamton-aquib/staline.nvim",
    event = { "BufReadPost", "BufNewFile" },
    dependencies = { "devicons" },
    config = function()
      -- for staline
      vim.opt.laststatus = 3

      local git_status = function(type, prefix)
        local status = vim.b.gitsigns_status_dict
        if not status then
          return nil
        end
        if not status[type] or status[type] == 0 then
          return nil
        end
        return prefix .. status[type]
      end

      local icons = require("zenedit.icons").icons

      require("staline").setup {
        defaults = {
          full_path   = true,
          line_column = "[%l/%L] 並%p%% ", -- `:h stl` to see all flags.
          true_colors = true,             -- true lsp colors.
          cool_symbol = "󰣇 ",          -- Change this to override default OS icon.
        },
        mode_colors = {
          n = "#89b4fa",
          i = "#a6e3a1",
          c = "#94e2d5",
          v = "#fab387",
          V = "#fab387",
          x = "#fab387",
          t = "#a6e3a1",
        },
        mode_icons = {
          n = "Normal",
          i = "Insert",
          c = "Command",
          v = "Visual",
          V = "V-Line",
          x = "V-Block",
          t = "Terminal",
        },
        lsp_symbols = icons.diagnostics,
        sections = {
          left  = { "- ", "-mode", "left_sep_double", " ", "branch",
            " ",
            {
              "GitSignsAdd",
              function()
                return git_status("added", icons.git.added) or ''
              end
            },
            " ",
            {
              "GitSignsChange",
              function()
                return git_status("changed", icons.git.modified) or ''
              end
            },
            " ",
            {
              "GitSignsDelete",
              function()
                return git_status("removed", icons.git.removed) or ''
              end
            }
          },
          mid   = { "file_name" },
          right = { "lsp", " ", "lsp_name", " ", "right_sep_double", "-line_column" },
        },
        inactive_sections = {
          left  = { "branch" },
          mid   = { "file_name" },
          right = { "line_column" }
        },
        special_table = {
          ["neo-tree"] = { "NeoTree", " " },
          ["lazy"] = { "Lazy", " " },
          ["mason"] = { "Mason", " " },
          ["help"] = { "Help", "󰋗 " },
          ["lspinfo"] = { "LspInfo", " " },
          ["man"] = { "Man", " " },
          ["checkhealth"] = { "CheckHealth", "󰗶 " },
          ["spectre_panel"] = { "Spectre", "" },
        },
      }

      -- for stabline
      vim.opt.showtabline = 2

      require('stabline').setup {
        style = "slant",
        stab_right = "",
        exclude_fts = { 'neo-tree', 'spectre_panel', 'help', 'man' },
        stab_start = function()
          if #vim.tbl_filter(function(b) return vim.bo[b].ft == "neo-tree" end, vim.api.nvim_list_bufs()) > 0 then
            return (" "):rep(require("neo-tree").config.window.width)
          end
        end
      }
    end
  },
}
