return {
  -- Color highlighter
  {
    "NvChad/nvim-colorizer.lua",
    event = { "BufReadPost", "BufNewFile" },
    opts = {
      filetypes = {
        "css",
        "javascript",
        "html",
        "php",
        "python",
        "scss",
        "sass",
        "yaml",
      },
      user_default_options = {
        AARRGGBB = false,
        css_fn = true,
        css = true,
        hsl_fn = true,
        mode = "background",
        names = false,
        rgb_fn = true,
        RGB = true,
        RRGGBBAA = true,
        RRGGBB = true,
        sass = { enable = true, parsers = { "css" }, },
        tailwind = true,
        virtualtext = "■",
      },
      buftypes = {},
    }
  },

  -- Indent highlighter
  {
    "lukas-reineke/indent-blankline.nvim",
    event = { "BufReadPost", "BufNewFile" },
    opts = {
      char = "▏",
      filetype_exclude = {
        "help",
        "alpha",
        "dashboard",
        "neo-tree",
        "Trouble",
        "lazy",
        "mason",
        "notify",
        "toggleterm",
      },
      show_current_context_start = true,
      show_current_context = false,
      show_end_of_line = true,
      show_first_indent_level = false,
      show_trailing_blankline_indent = false,
      space_char_blankline = " ",
      use_treesitter = true,
    },
  },
  {
    "HiPhish/nvim-ts-rainbow2",
    event = { "BufReadPost", "BufNewFile" },
  }
}
