-- ┏━┓╻ ╻╺┳╸┏━┓┏━╸┏┳┓╺┳┓┏━┓
-- ┣━┫┃ ┃ ┃ ┃ ┃┃  ┃┃┃ ┃┃┗━┓
-- ╹ ╹┗━┛ ╹ ┗━┛┗━╸╹ ╹╺┻┛┗━┛

local function augroup(name)
  return vim.api.nvim_create_augroup("zenedit_" .. name, { clear = true })
end

-- Highlight on yank
vim.api.nvim_create_autocmd("TextYankPost", {
  group = augroup("highlight_yank"),
  callback = function()
    vim.highlight.on_yank({ timeout = 500 })
  end,
})

-- go to last loc when opening a buffer
vim.api.nvim_create_autocmd("BufReadPost", {
  group = augroup("last_loc"),
  callback = function()
    local mark = vim.api.nvim_buf_get_mark(0, '"')
    local lcount = vim.api.nvim_buf_line_count(0)
    if mark[1] > 0 and mark[1] <= lcount then
      pcall(vim.api.nvim_win_set_cursor, 0, mark)
    end
  end,
})

-- close some filetypes with <q>
vim.api.nvim_create_autocmd("FileType", {
  group = augroup("close_with_q"),
  pattern = {
    "PlenaryTestPopup",
    "help",
    "lspinfo",
    "man",
    "notify",
    "checkhealth",
    "spectre_panel",
  },
  callback = function(event)
    vim.bo[event.buf].buflisted = false
    vim.keymap.set("n", "q", "<cmd>close<cr>",
      { buffer = event.buf, silent = true })
  end,
})

-- Highlight Variables using document_highlight
vim.api.nvim_create_autocmd("CursorHold", {
  group = augroup("document_highlight_on"),
  callback = function()
    -- If cursor is not on a word exit
    local cursor_word = vim.fn.expand("<cword>")
    if not cursor_word then
      return
    end
    -- If LSP is not ready exit
    if not vim.lsp.buf.server_ready() then
      return
    end
    local clients = vim.lsp.get_active_clients()
    for _, client in ipairs(clients) do
      -- Check if documentHighlight Capability is present in LSP
      if client.server_capabilities.documentHighlightProvider then
        vim.lsp.buf.document_highlight()
        return
      end
    end
  end,
})

-- Remove Highlight Variables
vim.api.nvim_create_autocmd("CursorMoved", {
  group = augroup("document_highlight_off"),
  callback = function()
    vim.defer_fn(function()
      if vim.lsp.buf.server_ready() then
        vim.lsp.buf.clear_references()
      end
    end, 100) -- Delay to avoid LSP errors while closing buffers
  end,
})
