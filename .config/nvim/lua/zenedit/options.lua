-- ┏━┓┏━┓╺┳╸╻┏━┓┏┓╻┏━┓
-- ┃ ┃┣━┛ ┃ ┃┃ ┃┃┗┫┗━┓
-- ┗━┛╹   ╹ ╹┗━┛╹ ╹┗━┛

-- Use set variable as vim options setter
local set         = vim.opt

-- [[Annoyances]]
set.fillchars.eob = " " -- show empty lines at the end of a buffer as ` ` {default `~`}

-- [[Fixes]]
set.splitkeep     = "screen"
set.signcolumn    =
"yes"                                                           -- always show the sign column, otherwise it would shift the text each time
set.spelllang     = { "en" }
set.iskeyword:append("-")                                       -- treats words with `-` as single words
set.nrformats:append("alpha")                                   -- treats alphabets as numbers which can be incremented and decremented
set.completeopt = { "menu", "menuone", "noselect", "noinsert" } -- mostly just for cmp
set.shortmess:append { W = true, I = true, c = true }           -- hide all the completion messages, e.g. "-- XXX completion (YYY)", "match 1 of 2", "The only match", "Pattern not found"
set.whichwrap:append("<,>,[,],h,l")                             -- keys allowed to move to the previous/next line when the beginning/end of line is reached
set.conceallevel   = 3                                          -- Conceal Level

-- [[Editor]]
set.number         = true  -- Show Numbers
set.relativenumber = false -- Show Relative Numbers
set.ruler          = false -- hide the line and column number of the cursor position
set.cursorline     = true  -- highlight the current line
set.showtabline    = 0     -- always hide tabs
set.scrolloff      = 8     -- minimal number of screen lines to keep above and below the cursor
set.sidescrolloff  = 8     -- minimal number of screen columns to keep to the left and right of the cursor if wrap is `false`
set.numberwidth    = 4     -- minimal number of columns to use for the line number {default 4}
set.showmode       = false -- we don't need to see things like -- INSERT -- anymore
set.laststatus     = 0     -- only the last window will always have a status line
set.cmdheight      = 0     -- more space in the neovim command line for displaying messages
set.list           = true  -- Show some invisible characters (tabs...
-- set.colorcolumn    = ""  -- Show colorcolumn after 80 lines

-- [[ Search ]]
set.ignorecase     = true  -- ignore case in search patterns
set.smartcase      = true  -- smart case
set.hlsearch       = false -- highlight all matches on previous search pattern
set.incsearch      = true  -- Use incremental search

-- [[Indenting]]
set.wrap           = false -- display lines as one long line
set.shiftround     = true  -- Round Indent
set.breakindent    = true  -- virtual line indent
set.smartindent    = true  -- make indenting smarter again

-- [[Split]]
set.splitbelow     = true -- force all horizontal splits to go below current window
set.splitright     = true -- force all vertical splits to go to the right of current window

-- [[Folding]]
set.foldmethod     = 'expr'
set.foldexpr       = 'nvim_treesitter#foldexpr()'

-- [[Editor Config]]
set.encoding       = 'utf8' -- String encoding to use
set.fileencoding   = 'utf8' -- File encoding to use
set.expandtab      = true   -- convert tabs to spaces
set.shiftwidth     = 2      -- the number of spaces inserted for each indentation
set.tabstop        = 2      -- insert 2 spaces for a tab
set.softtabstop    = 2      -- insert 2 spaces for softtabstop

-- [[UI]]
set.termguicolors  = true            -- set term gui colors (most terminals support this)
set.background     = "dark"          -- set dark colorscheme
set.guifont        = "monospace:h12" -- the font used in graphical neovim applications

-- [[Coding]]
set.colorcolumn    = "80" -- highlight column for better coding style
set.syntax         = "ON" -- Set theme on

-- [[System]]
set.clipboard      = "unnamedplus"                         -- allows neovim to access the system clipboard
set.mouse          = "a"                                   -- allow the mouse to be used in neovim
set.mousemodel     = "extend"                              -- allow the mouse to be used in neovim
set.undofile       = true                                  -- enable persistent undo
set.undolevels     = 10000
set.undodir        = vim.fn.expand('~/.cache/vim/undodir') -- set explicit undodir
set.backup         = false                                 -- creates a backup file
set.writebackup    = false                                 -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
set.swapfile       = false                                 -- creates a swapfile
set.updatetime     = 200                                   -- faster completion (4000ms default)
set.timeoutlen     = 300                                   -- time to wait for a mapped sequence to complete (in milliseconds)

-- [[Performance]]
set.showcmd        = false -- hide (partial) command in the last line of the screen (for performance)
set.lazyredraw     = true

-- [[Commands]]
vim.cmd([[set nofoldenable]]) -- do not fold by default
