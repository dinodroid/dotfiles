#
# ~/.zprofile
#

export ZDOTDIR="$HOME/.config/zsh"

[[ -f "$ZDOTDIR/.zshrc" ]] && . "$ZDOTDIR/.zshrc"

if [[ "$(tty)" = "/dev/tty1" ]]; then
	pgrep qtile || startx "$HOME/.xinitrc"
fi

