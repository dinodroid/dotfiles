--╻ ╻┏━╸╺━┓╺┳╸┏━╸┏━┓┏┳┓
--┃╻┃┣╸ ┏━┛ ┃ ┣╸ ┣┳┛┃┃┃
--┗┻┛┗━╸┗━╸ ╹ ┗━╸╹┗╸╹ ╹

local wezterm = require 'wezterm'
local config = {}

config.font = wezterm.font_with_fallback {
  'Maple Mono SC NF',
  'JetBrains Mono Nerd Font Bold',
  'Fantasque Sans Mono Bold',
  'RobotoMono Nerd Font Bold',
  'Fira Code',
}

config.font_size = 15
config.cell_width = 0.9
config.line_height = 1.0
config.freetype_load_target = "Light"
-- config.freetype_render_target = "HorizontalLcd"
config.freetype_load_flags = "NO_HINTING"
config.front_end = "OpenGL"
config.color_scheme = 'Catppuccin Mocha'
config.enable_tab_bar = false

-- Donot Ask for confirmation before closing
config.window_close_confirmation = "NeverPrompt"

return config
