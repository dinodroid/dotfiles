return {

 -- Git Integration
  {
    "lewis6991/gitsigns.nvim",
    dependencies = { "plenary" },
    event = { "BufReadPost", "BufNewFile" },
    opts = {
      signs = {
        add = { text = "▎" },
        delete = { text = "" },
        topdelete = { text = "" },
        changedelete = { text = "▎" },
        untracked = { text = "▎" },
      }
    }
  },

  -- Terminal Integration
  {
    'akinsho/toggleterm.nvim',
    version = "*",
    event = { "BufReadPost", "BufNewFile" },
    config = function()
      local opts = {
        size = 14,
        open_mapping = [[<C-\>]],
        shade_terminal = true,
        shading_factor = 1,
        start_in_insert = true,
        direction = 'horizontal'
      }
      require("toggleterm").setup(opts)

      function _G.set_terminal_keymaps()
        opts = { noremap = true }
        vim.api.nvim_buf_set_keymap(0, "t", "<C-h>", [[<C-\><C-n><C-W>h]], opts)
        vim.api.nvim_buf_set_keymap(0, "t", "<C-j>", [[<C-\><C-n><C-W>j]], opts)
        vim.api.nvim_buf_set_keymap(0, "t", "<C-k>", [[<C-\><C-n><C-W>k]], opts)
        vim.api.nvim_buf_set_keymap(0, "t", "<C-l>", [[<C-\><C-n><C-W>l]], opts)
      end

      vim.cmd "autocmd! TermOpen term://* lua set_terminal_keymaps()"
      local Terminal = require("toggleterm.terminal").Terminal

      -- Add Lazygit terminal
      local lazygit = Terminal:new { cmd = "lazygit", hidden = true, direction = "float" }
      function _LAZYGIT_TOGGLE()
        lazygit:toggle()
      end
    end,
    keys = {
      { "<leader>tt", "<cmd>ToggleTerm size=14 dir=%:p:h<cr>",                      desc = "term" },
      { "<leader>tf", "<cmd>ToggleTerm size=40 dir=%:p:h direction=float<cr>",      desc = "term float" },
      { "<leader>th", "<cmd>ToggleTerm size=14 dir=%:p:h direction=horizontal<cr>", desc = "term horizontal" },
      { "<leader>tv", "<cmd>ToggleTerm size=50 dir=%:p:h direction=vertical<cr>",   desc = "term vertical" },
      { "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>",                             desc = "term lazygit" },
    },
  },

  -- TMUX Integration
  {
    "christoomey/vim-tmux-navigator",
    event = "VeryLazy",
    cond = function ()
      -- Only enable when opened in tmux
      local term = os.getenv("TERM")
      if term and string.find(term, "tmux") then
        return true
      else
        return false
      end
    end
  }
}
