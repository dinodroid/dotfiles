return {
  -- Fuzzy Navigation
  {
    "ibhagwan/fzf-lua",
    dependencies = { "devicons" },
    cmd = "FzfLua",
    keys = {
      { "<C-p>",      "<cmd>FzfLua files winopts.title='Files'<cr>", desc = "find files" },
      { "<leader>fb", "<cmd>FzfLua buffers<cr>",                     desc = "buffers" },
      { "<leader>ff", "<cmd>FzfLua files cwd=%:p:h<cr>",             desc = "find files (cwd)" },
      { "<leader>fg", "<cmd>FzfLua grep cwd=%:p:h<cr>",              desc = "find in files (Grep)" },
      { "<leader>fh", "<cmd>FzfLua help_tags<cr>",                   desc = "help pages" },
      { "<leader>fm", "<cmd>FzfLua man_pages<cr>",                   desc = "man pages" },
      { "<leader>fo", "<cmd>FzfLua oldfiles<cr>",                    desc = "recent files" },
      { "<leader>ft", "<cmd>FzfLua colorschemes<cr>",                desc = "colorschemes" },
      { "<leader>fq", "<cmd>FzfLua quickfix<cr>",                    desc = "quickfix list" },
      -- Git
      { "<leader>gs", "<cmd>FzfLua git_status<cr>",                  desc = "git status" },
      -- LSP
      { "ga",         "<cmd>FzfLua lsp_finder<cr>",                  desc = "all lsp definitions and references" },
      { "gr",         "<cmd>FzfLua lsp_references<cr>",              desc = "lsp references" },
      { "gd",         "<cmd>FzfLua lsp_definitions<cr>",             desc = "lsp definition" },
      { "gD",         "<cmd>FzfLua lsp_declarations<cr>",            desc = "lsp declarations" },
      { "gi",         "<cmd>FzfLua lsp_implementations<cr>",         desc = "lsp implementations" },
      { "go",         "<cmd>FzfLua lsp_typedefs<cr>",                desc = "lsp type definitions" },
      { "<leader>td", "<cmd>FzfLua lsp_document_diagnostics<cr>",    desc = "lsp document diagnostics" },
      { "<leader>tw", "<cmd>FzfLua lsp_workspace_diagnostics<cr>",   desc = "lsp workspace diagnostics" },
      { "<leader>ca", "<cmd>FzfLua lsp_code_actions<cr>",            desc = "lsp code actions" },
    },
    config = function()
      local img_previewer = vim.fn.executable("ueberzug") == 1 and { "ueberzug" } or { "viu", "-b" }
      local lsp_symbols = require("zenedit.icons").icons.kinds

      require('fzf-lua').setup({
        profile    = "fzf-native",
        width      = 0.85,
        grep       = {
          rg_opts = "--sort-files --hidden --column --line-number --no-heading " ..
              "--color=always --smart-case -g '!{.git,node_modules,vendor}/*'",
        },
        files      = {
          fd_opts = "--type f --exclude node_modules --exclude .git --exclude vendor",
        },
        git        = {
          status = {
            previewer = "git_diff",
            preview_pager = "delta --width=$FZF_PREVIEW_COLUMNS",
          }
        },
        lsp        = {
          symbols = lsp_symbols,
        },
        border     = "rounded",
        fzf_opts   = {
          ['--prompt'] = " ",
          ['--pointer'] = " ",
        },
        winopts    = {
          title = "Find",
          title_pos = "center",
          preview = {
            default     = "bat",
            border      = "noborder",
            title_align = "center",
            delay       = 10,
            horizontal  = 'right:50%',
          },
        },
        previewers = {
          bat = {
            theme = "Catppuccin-mocha"
          },
          git_diff = {
            pager = "delta --width=$FZF_PREVIEW_COLUMNS",
          },
          builtin = {
            ueberzug_scaler = "cover",
            extensions = {
              ["gif"] = img_previewer,
              ["png"] = img_previewer,
              ["jpg"] = img_previewer,
              ["jpeg"] = img_previewer,
            },
          },
        },
      })

      -- Redraw Fzf-if window is resized
      vim.api.nvim_create_autocmd("VimResized", {
        pattern = '*',
        command = 'lua require("fzf-lua").redraw()'
      })
    end
  },
  -- File Tree Navigation
  {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v2.x",
    cmd = "Neotree",
    dependencies = { "plenary", "devicons" },
    keys = {
      { "<leader>e", "<cmd>Neotree toggle dir=%:p:h:h source=filesystem<CR>", desc = "neotree (cwd)" },
    },
    init = function()
      vim.g.neo_tree_remove_legacy_commands = 1
      if vim.fn.argc() == 1 then
        local stat = vim.loop.fs_stat(vim.fn.argv(0))
        if stat and stat.type == "directory" then
          require("neo-tree")
        end
      end
    end,
    opts = {
      sources = {
        "filesystem", -- Neotree filesystem source
      },
      filesystem = {
        bind_to_cwd = false,
        follow_current_file = true,
        use_libuv_file_watcher = true,
      },
      close_if_last_window = false,
      enable_diagnostics = true,
      enable_git_status = true,
      popup_border_style = "rounded",
      sort_case_insensitive = false, -- used when sorting files and directories in the tree
      default_component_configs = {
        indent = {
          with_expanders = true, -- if nil and file nesting is enabled, will enable expanders
          expander_collapsed = "",
          expander_expanded = "",
          expander_highlight = "NeoTreeExpander",
        },
      },
      window = {
        mappings = {
          -- Reveal Info
          ['<tab>'] = function(state)
            local node = state.tree:get_node()
            if require("neo-tree.utils").is_expandable(node) then
              state.commands["toggle_node"](state)
            else
              state.commands['open'](state)
              vim.cmd('Neotree reveal')
            end
          end,
          -- HJLK Navigation
          ["h"] = function(state)
            local node = state.tree:get_node()
            if node.type == 'directory' and node:is_expanded() then
              require 'neo-tree.sources.filesystem'.toggle_directory(state, node)
            else
              require 'neo-tree.ui.renderer'.focus_node(state, node:get_parent_id())
            end
          end,
          ["l"] = function(state)
            local node = state.tree:get_node()
            if node.type == 'directory' then
              if not node:is_expanded() then
                require 'neo-tree.sources.filesystem'.toggle_directory(state, node)
              elseif node:has_children() then
                require 'neo-tree.ui.renderer'.focus_node(state, node:get_child_ids()[1])
              end
            end
          end,
        },
      },
    },
  }
}
