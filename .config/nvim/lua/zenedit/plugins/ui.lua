return {
  {
    "folke/noice.nvim",
    event = { "BufReadPost", "BufNewFile" },
    dependencies = { "nui", "notify" },
    init = function()
      vim.opt.lazyredraw = false
    end,
    opts = {
      presets = {
        bottom_search = true,
        command_palette = true,
        long_message_to_split = true,
        inc_rename = true,
      },
    },
  },
  {
    "MunifTanjim/nui.nvim",
    name = "nui",
  },

  {
    "rcarriga/nvim-notify",
    name = "notify",
    event = { "BufReadPost", "BufNewFile" },
    keys = {
      {
        "<leader>un",
        function()
          require("notify").dismiss({ silent = true, pending = true })
        end,
        desc = "Delete all Notifications",
      },
    },
    opts = {
      lsp = {
        hover = {
          enabled = false
        },
        signature = {
          enabled = false
        },
        override = {
          ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
          ["vim.lsp.util.stylize_markdown"] = true,
          ["cmp.entry.get_documentation"] = true,
        },
      },
      timeout = 2500,
      max_height = function()
        return math.floor(vim.o.lines * 0.75)
      end,
      max_width = function()
        return math.floor(vim.o.columns * 0.75)
      end,
    },
  },

  -- Dressing
  {
    "stevearc/dressing.nvim",
    event = { "BufReadPost", "BufNewFile" },
    init = function()
      local lazy = require("lazy")
      vim.ui.select = function(...)
        lazy.load({ plugins = { "dressing.nvim" } })
        return vim.ui.select(...)
      end
      vim.ui.input = function(...)
        lazy.load({ plugins = { "dressing.nvim" } })
        return vim.ui.input(...)
      end
    end,
  }
}
