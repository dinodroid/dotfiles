#####################################
# Qtile Configuration
# ~/.config/qtile/config.py
####################################

from typing import List
from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.command import lazy
# Import system process
import os
import subprocess

mod = "mod4"
home=os.path.expanduser('~')

## Commands----------------------------------------------------
class cmd:
    terminal        = 'alacritty'
    musicplayer     = 'ncmpcpp',
    editor          = 'codium',
    editor2         = 'nvim ~',
    explorer        = 'pcmanfm'
    browser         = 'brave'
    browser_private = 'brave --incognito'
    windowmenu      = 'rofi -show window'
    runmenu         = 'rofi -show run'
    programmenu     = 'rofi -show drun'
    locker          = 'slock'
    def run(scriptname):
        return os.path.join(home, '.scripts/'+ scriptname)

# █▄▀ █▀▀ █▄█ █▄▄ █ █▄░█ █▀▄ █▀
# █░█ ██▄ ░█░ █▄█ █ █░▀█ █▄▀ ▄█

keys = [
    # Switch between windows
    Key([mod], "h",     lazy.layout.shrink(),lazy.layout.increase_nmaster(),desc='Shrink master window'),
    Key([mod], "l",     lazy.layout.grow(),lazy.layout.decrease_nmaster(),  desc='Grow window'),
    Key([mod], "Right", lazy.layout.down(),                                 desc="Move focus down"),
    Key([mod], "Left",  lazy.layout.up(),                                   desc="Move focus up"),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(),            desc="Toggle between layouts"),
    Key([mod], "f",     lazy.window.toggle_floating(), desc="Toggle between layouts"),
    Key([mod], "q",     lazy.window.kill(),            desc="Kill focused window"),

    Key([mod], "bracketleft", lazy.to_screen(0), desc="switch to screen 0"),
    Key([mod], "bracketright", lazy.to_screen(1), desc="switch to screen 1"),

    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h",    lazy.layout.shuffle_left(),  desc="Move window to the left"),
    Key([mod, "shift"], "l",    lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "Right",lazy.layout.shuffle_down(),  desc="Move window down"),
    Key([mod, "shift"], "Left", lazy.layout.shuffle_up(),    desc="Move window up"),

    # Move between groups
    Key([mod], 'Down',          lazy.screen.prev_group(),    desc="Move to previous group"),
    Key([mod], 'Up',            lazy.screen.next_group(),    desc="Move to next group"),
    Key([mod, "control"], "r",  lazy.restart(),              desc="Restart qtile"),
    Key([mod, "control"], "q",  lazy.shutdown(),             desc="Shutdown qtile"),
]

# █▀▀ █▀█ █▀█ █░█ █▀█ █▀
# █▄█ █▀▄ █▄█ █▄█ █▀▀ ▄█

group_names = [
        ("爵", {'layout':  'monadtall'}),
        ("", {'layout':  'monadtall'}),
        ("", {'layout':  'monadtall'}),
        ("", {'layout':  'monadtall'}),
        ("אַ",{'layout':  'monadtall'}),
        ("", {'layout':  'monadtall'}),
        ("﬐", {'layout':  'monadtall'}),
        ("", {'layout':  'monadtall'}),
        ("契",{'layout':  'monadtall'})
    ]
groups = [Group(name, **kwargs) for name, kwargs in group_names]


for i, (name, kwargs) in enumerate(group_names, 1):
    # Switch to another group
    keys.append(Key([mod], str(i), lazy.group[name].toscreen(), desc="Switch to group {}".format(name)))
    # Send current window to another group
    keys.append(Key([mod, "shift"], str(i),lazy.window.togroup(name),lazy.group[name].toscreen(),
    desc="Switch to & move focused window to group {}".format(name)))

## Colors-----------------------------------------------------

# COLORS FOR THE BAR - Catppuccin Colors
def get_colors():
    return [
        ["#1e1e2e", "#1e1e2e"],
        ["#6e6c7e", "#6e6c7e"],
        ["#F28FAD", "#F28FAD"],
        ["#ABE9B3", "#ABE9B3"],
        ["#FAE3B0", "#FAE3B0"],
        ["#96CDFB", "#96CDFB"],
        ["#DDB6F2", "#DDB6F2"],
        ["#F5C2E7", "#F5C2E7"],
        ["#C3BAC6", "#C3BAC6"],
        ["#988BA2", "#988BA2"],
        ["#F28FAD", "#F28FAD"],
        ["#ABE9B3", "#ABE9B3"],
        ["#FAE3B0", "#FAE3B0"],
        ["#96CDFB", "#96CDFB"],
        ["#DDB6F2", "#DDB6F2"],
        ["#D9E0EE", "#D9E0EE"],
    ]
col = get_colors()
transparent="#000000ab"
alertcolor=col[2]

## Fonts------------------------------------------------------

fonts = [
    "ComicMono Nerd Font",
    "CaskaydiaCove Nerd Font Mono Regular",
    "JetBrains Mono Nerd Font Bold",
    "Fantasque Sans Mono Bold",
    "RobotoMono Nerd Font Bold",
]

layout_defaults = dict(
    margin=8,
    border_width=2,
    border_focus=col[5][0],
    border_normal=col[3][0],
    grow_amount=10
)

layouts = [
    layout.MonadTall(**layout_defaults),
    layout.Stack(num_stacks=2, **layout_defaults),
    layout.Max(**layout_defaults),
    layout.Floating(**layout_defaults),
    #layout.Columns(border_focus_stack=['#d75f5f', '#8f3d3d'], border_width=4),
    #layout.Bsp(),
    # layout.Matrix(),
    #layout.MonadWide(),
    #layout.RatioTile(),
    #layout.Tile(),
    #layout.TreeTab(),
    #layout.VerticalTile(),
    #layout.Zoomy(),
]
widget_defaults = dict(
    font=fonts[0],
    fontsize=16,
    padding=6,
    background=col[1],
    color=col[15]
)

extension_defaults = widget_defaults.copy()

def icon(icon_text, color, backcolor, fontsize=16, padding=5):
    return widget.TextBox(
        font=fonts[0],
        text=icon_text,
        foreground=color,
        background=backcolor,
        padding=padding,
        fontsize=fontsize
    )

def customscript(color1, color2, update, script, mousecallbacks):
    return widget.GenPollText(
        foreground=color1,
        background=color2,
        update_interval=update,
        func=lambda:subprocess.check_output(cmd.run(script)).decode("utf-8"),
        mouse_callbacks=mousecallbacks,
        padding=10
    )

# Mouse Callbacks Functions
def runscript(script):
    return lambda:qtile.cmd_spawn(script)

# █▄▄ ▄▀█ █▀█
# █▄█ █▀█ █▀▄

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(
                    scale=0.75,
                    custom_icon_paths = [os.path.expanduser("~/.config/qtile/layout_icons")],
                    background=transparent
                ),
                widget.GroupBox(
                    disable_drag=True,
                    active=col[3],
                    inactive=col[1],
                    highlight_method ="line",
                    rounded="true",
                    this_current_screen_border=col[2],
                    urgent_border=alertcolor,
                    foreground=col[1],
                    background=transparent,
                    padding=2,
                    spacing=0,
                    borderwidth=5,
                    fontsize=16,
                    font=fonts[0]
                ),
                widget.Spacer(width=bar.STRETCH,background=transparent),

                icon(' ', col[3] , transparent, 16, 8),
                widget.Clock(format="%d %a %I:%M %p",
                    foreground=col[15],
                    background=transparent,
                    font=fonts[0],
                    mouse_callbacks={"Button1": runscript(cmd.run('calender')) }
                ),

                widget.Spacer(width=bar.STRETCH,background=transparent),

                icon('', col[2] , transparent, 16, 8),
                customscript(col[15], transparent, 300, "openweather", {}),

                icon('墳', col[5], transparent),
                widget.PulseVolume(
                    foreground=col[15],
                    background=transparent,
                    mouse_callbacks={"Button1" : runscript('pavucontrol') }
                ),
                icon('', col[3], transparent),
                widget.Battery(
                    font=fonts[0],
                    foreground=col[15],
                    background=transparent,
                    charge_char='ﮣ',
                    discharge_char='-',
                    full_char="",
                    format="{char} {percent:2.0%}",
                    show_short_text=False,
                    low_foreground=alertcolor,
                    low_percentage=0.2,
                    update_interval=1,
                    mouse_callbacks={"Button1" : runscript('xfce4-power-manager-settings')}
                ),

                widget.Systray(icon_size=20, padding=0, background=col[0], font=fonts[0]),
                widget.QuickExit(
                    font=fonts[0],
                    background=transparent,
                    foreground=col[2],
                    default_text='  ',
                    countdown_start=5,
                    countdown_format ='{}s'
                ),
            ],
            32, opacity=1, margin=[0, 0, 4, 0], background=transparent
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(wm_class='pavucontrol'),  # pavucontrol
    Match(wm_class='xfce4-power-manager-settings'),  # pavucontrol
    Match(wm_class='galculator'),  # pavucontrol
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry

])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# For java UI toolkits;
wmname = "LG3D"

# Autostart Qtile
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])
