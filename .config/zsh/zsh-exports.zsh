# XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_STATE_HOME=${XDG_CONFIG_HOME:="$HOME/.local/state"}

# PATH OF MY SCRIPTS
export PATH="$PATH:$HOME/.scripts:$HOME/.local/bin"

# Default Apps
export TERMINAL="alacritty"
export EDITOR="nvim"
export VISUAL="nvim"
export BROWSER="librewolf"
export TRUE_BROWSER="librewolf"
export VIDEO="mpv"
export IMAGE="sxiv -a"
export COLORTERM="truecolor"
export OPENER="linkhandler"
export PAGER="less"
export WM="qtile"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# Control History
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000
export HISTFILE="$HOME/.cache/zsh/history"

# for fzf catpuccin colors
export FZF_DEFAULT_OPTS='--color=bg+:#302D41,bg:#1E1E2E,spinner:#F8BD96,hl:#F28FAD,fg:#D9E0EE,header:#F28FAD,info:#DDB6F2,pointer:#F8BD96,marker:#F8BD96,fg+:#F2CDCD,prompt:#DDB6F2,hl+:#F28FAD,gutter:#1e1e2e,label:italic:black --reverse --prompt=" " --pointer=" " --marker=" " --border=rounded --margin 5% --border-label="╢ $label ╟" --no-separator'
# for micro truecolor
export MICRO_TRUECOLOR="1"

# for WEZTERM
export WEZTERM_CONFIG_FILE="$XDG_CONFIG_HOME/wezterm/wezterm.lua"

