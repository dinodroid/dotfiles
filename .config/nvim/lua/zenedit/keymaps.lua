-- ╻┏ ┏━╸╻ ╻┏┳┓┏━┓┏━┓┏━┓
-- ┣┻┓┣╸ ┗┳┛┃┃┃┣━┫┣━┛┗━┓
-- ╹ ╹┗━╸ ╹ ╹ ╹╹ ╹╹  ┗━┛

-- Create remap function

local function remap(mode, lhs, rhs, desc, expr)
  local opts = { noremap = true, silent = true, desc = desc, expr = false }
  if expr then
    opts.expr = true
  end
  vim.keymap.set(mode, lhs, rhs, opts)
end

--Remap space as leader key
remap("", "<Space>", "<Nop>", "leader remapping")
vim.g.mapleader = " "
vim.g.maplocalleader = " "

------------------------------------------------------------------------------------
-- Windows / Splits
--
-- Move to window using the <ctrl> hjkl keys
remap("n", "<C-h>", "<C-w>h", "Go to left window")
remap("n", "<C-j>", "<C-w>j", "Go to lower window")
remap("n", "<C-k>", "<C-w>k", "Go to upper window")
remap("n", "<C-l>", "<C-w>l", "Go to right window")

-- Resize window using <ctrl> arrow keys
remap("n", "<C-Up>", "<cmd>resize +2<cr>", "Increase window height")
remap("n", "<C-Down>", "<cmd>resize -2<cr>", "Decrease window height")
remap("n", "<C-Left>", "<cmd>vertical resize -2<cr>", "Decrease window width")
remap("n", "<C-Right>", "<cmd>vertical resize +2<cr>", "Increase window width")
------------------------------------------------------------------------------------
-- Buffers
--
-- Navigate buffers
remap("n", "<S-l>", "<cmd>bnext<cr>", "Move to right buffer")
remap("n", "<S-h>", "<cmd>bprevious<cr>", "Move to left buffer")
remap("n", "<BS>", "<cmd>b#<cr>", "Move to last buffer")

------------------------------------------------------------------------------------
-- Press jk fast to enter
remap("i", "jk", "<esc>", "go to normal mode")
remap("i", "kj", "<esc>", "go to normal mode")

-- Better paste
remap("v", "p", '"_dP', "paste")

-- Better Indenting
remap("v", "<", "<gv", "indent to left")
remap("v", ">", ">gv", "indent to right")

-- better up/down
remap("n", "j", "v:count == 0 ? 'gj' : 'j'", "Move Down one line", { expr = true, silent = true })
remap("n", "k", "v:count == 0 ? 'gk' : 'k'", "Move Up one line", { expr = true, silent = true })

-- Move Lines
remap("n", "<A-j>", "<cmd>m .+1<cr>==", "Move down")
remap("n", "<A-k>", "<cmd>m .-2<cr>==", "Move up")
remap("i", "<A-j>", "<esc><cmd>m .+1<cr>==gi", "Move down")
remap("i", "<A-k>", "<esc><cmd>m .-2<cr>==gi", "Move up")
remap("v", "<A-j>", ":m '>+1<cr>gv=gv", "Move down")
remap("v", "<A-k>", ":m '<-2<cr>gv=gv", "Move up")
remap("v", "<S-j>", ":m '>+1<cr>gv=gv", "Move down")
remap("v", "<S-k>", ":m '<-2<cr>gv=gv", "Move up")

-- Save and go to normal mode
remap({ "n", "i", "v", "x" }, "<C-s>", "<cmd>w<cr><esc>", "Save file")

-- Go to visual to insert mode directly
remap('v', "i", "<esc>i", "go to insert mode")

-- Allow gf to open non-existent files
remap('', 'gf', ':edit <cfile><cr>', "edit path as file")

-- Clear search with <esc>
remap({ "n", "i" }, "<esc>", "<cmd>noh<cr><esc>", "Escape and clear hlsearch")

-- Use Enter in normal mode to add a blank line
remap("n", "<cr>", "o<esc>", "New Line")

-- # Leader Navigation ---------------------------------------------------
--
-- Select all from file
remap('n', '<leader>a', '<cmd>keepjumps normal! ggVG<cr>', "select all text")

-- Remove trailing whitespace
remap("n", "<leader>sf", "<cmd>%s/\\s\\+$//e<cr>", "remove whitespaces")

-- Files
remap("n", "<leader>fn", "<cmd>ene <BAR> startinsert <cr>", "[F]ile [N]ew")

-- Toggle WordWrap
remap("n", "<leader>tw", "<cmd>set wrap!<cr>", "[T]oggle [W]rap")

-- Toggle Spellcheck
remap("n", "<leader>ts", "<cmd>set spell!<cr>", "[T]oggle [S]pell")

-- Buffers
remap("n", "<leader>bn", "<cmd>bnext<cr>", "[B]uffer [N]ext")
remap("n", "<leader>bp", "<cmd>bprevious<cr>", "[B]uffer [P]revious")

-- Lazy up
remap("n", "<leader>p", "<cmd>Lazy<cr>", "Open [P]ackage [M]anager")
remap("n", "<leader>m", "<cmd>Mason<cr>", "Open [M]ason")
